<?php

/**
 * @file
 * Support for migration into Location fields.
 */

class MigrateLocationFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('location'));
  }

  /*
   * Arguments for a Location field migration.
   */
  static function arguments() {
    return get_defined_vars();
  }

  /**
   * Convert incoming Location into the proper field arrays for Location fields.
   *
   * @param $entity
   *  The destination entity which will hold the field arrays.
   * @param array $field_info
   *  Metadata for the location field being populated.
   * @param array $instance
   *  Metadata for this instance of the location field being populated.
   * @param array $values
   *  Array of location values to be fielded.
   */
  public function prepare($entity, array $instance, array $values) {
        
    // file_put_contents('c:/php/temp/'.__class__.'_'. __function__ .'-instance.txt', "\n\n" . var_export($instance, TRUE), FILE_APPEND);
    // file_put_contents('c:/php/temp/'.__class__.'_'. __function__ .'-values.txt', "\n\n" . var_export($values, TRUE), FILE_APPEND);
    // file_put_contents('c:/php/temp/'.__class__.'_'. __function__ .'-entity.txt', "\n\n" . var_export($entity, TRUE), FILE_APPEND);
    
    foreach($values as $location) {
      if ($location{0} == '{') {
        // Handle JSON input
        $location = (array)json_decode($location);
      }
      
      
      // file_put_contents('c:/php/temp/'.__class__.'_'. __function__ .'-value.txt', "\n\n" . var_export($location, TRUE), FILE_APPEND);
      $data = array(
        'street' => $location['street'],
        'city' => $location['city'],
        'province' => $location['province'],
        'country' => $location['country'],
        'additional' => $location['additional'],
        'postal_code' => $location['postal_code'],
        'locpick' => array(
          'user_latitude' => $location['latitude'],
          'user_longitude' => $location['longitude'],
        ),
      );
      
      $return[] = $data;
    }
    
    return $return;
  }
}
